Pod::Spec.new do |s|


  s.name         = "InMobiSDK"
  s.version      = "7.0.1"
  s.summary      = "InMobi iOS SDK"
  s.description  = "This is the InMobi iOS SDK 7.0.1. Please proceed to http://www.inmobi.com/sdk for more information."
  s.homepage     = "http://www.inmobi.com"
  s.platform     = :ios, "8.0"
  s.license      = "Custom"
  s.author       = { "InMobi" => "sdk-dev-support@inmobi.com" }


  s.source              = { :http => "https://bitbucket.org/pacerhealth/inmobi-ios-sdk/raw/master/InMobi_iOS_SDK_7.0.1_new.tar.gz" }
  #s.source              = { :http => "http://127.0.0.1:8080/InMobi_iOS_SDK_7.0.1.tar.gz" }
  s.preserve_paths      = "InMobiSDK.framework"
  s.source_files = 'InMobiSDK.framework/Headers/'
  s.vendored_frameworks = "InMobiSDK.framework"
  s.frameworks          = "AdSupport", "AudioToolbox", "AVFoundation", "CoreTelephony", "CoreLocation", "Foundation", "MediaPlayer", "MessageUI", "StoreKit", "Social", "SystemConfiguration", "Security", "SafariServices", "UIKit"
  s.weak_frameworks     = "WebKit"
  s.libraries           = "sqlite3.0", "z"

  s.xcconfig      = { "OTHER_LDFLAGS" => "-ObjC" }

end
